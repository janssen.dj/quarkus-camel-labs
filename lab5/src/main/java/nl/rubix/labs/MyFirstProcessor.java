package nl.rubix.labs;


import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import nl.rubix.labs.model.Beer;

@Named("process")
@ApplicationScoped
public class MyFirstProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        String beerName = exchange.getIn().getHeader("name", String.class);
        Beer beer = new Beer(beerName, "delicious beer!");
        exchange.getIn().setBody(beer);
    }
}
