# lab 5

## Docker
```bash
mvn quarkus:add-extension -Dextensions="container-image-docker"

mvn clean package -Dquarkus.container-image.build=true

docker run  -p 8080:8080 djanssen/rubix-labs:1.0-SNAPSHOT
```


## Native image
Going native!!

Make sure you have a graalVM installed or a Docker instance running.

```bash
mvn package -Pnative

./target/RubixLabs-1.0-SNAPSHOT-runner 
__  ____  __  _____   ___  __ ____  ______ 
 --/ __ \/ / / / _ | / _ \/ //_/ / / / __/ 
 -/ /_/ / /_/ / __ |/ , _/ ,< / /_/ /\ \   
--\___\_\____/_/ |_/_/|_/_/|_|\____/___/   

```

Going native with docker!

```bash

mvn clean package -Dquarkus.container-image.build=true -Pnative

docker run  -p 8080:8080 djanssen/rubix-labs:1.0-SNAPSHOT
```