# Creating the project

Quarkus mainly uses maven archetypes to start projects. 

```bash
 mvn io.quarkus:quarkus-maven-plugin:1.8.3.Final:create \
    -DprojectGroupId=nl.rubix.labs \
    -DprojectArtifactId=lab2 \
    -Dextensions=camel-quarkus-log,camel-quarkus-timer
```

After the build run:

```bash
cd getting-started
mvn clean install

```