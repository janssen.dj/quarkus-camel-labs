# lab 6

First add the dependency:

```xml
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-junit5</artifactId>
      <scope>test</scope>
    </dependency>
```

Then create the test class.
* use the following annotation to start the test: io.quarkus.test.junit.QuarkusTest
* For testing start with the static: io.restassured.RestAssured.given;
* that is it!