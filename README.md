# Quarkus Camel Labs

This repo contains the Labs of a Quarkus - Camel hands on session. These labs take you in a few easy steps through setting up a project and creating camel routes. 

## Table of content
* lab 1 - Creating the project
* lab 2 - Creating the camel context
* lab 3 - Dependency injection
* lab 4 - Adding some rest
* lab 5 - Containers
* lab 6 - Testing

## Requirements

* JDK 8+ with JAVA_HOME configured appropriately
* Apache Maven 3.6.2+
* GraalVM and/or Docker
