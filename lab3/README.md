# lab3

## Part 1 Bean creation
Now that we have a working route we can start expanding it a bit. In this part we will be creating a bean and adding that bean to the route. 

* First create the bean by implementing a Processor
* Call the bean from the route
* Run the route

## Part 2 Debugging it!

Quarkus has a cool feature called hot deploy. This means that you can make changes, save them and simply wait a second for the changes to be deployed!

run:
```bash
mvn quarkus:dev
```

Wait and after a success full deploy make a small change and wait and see what happens.

In order to debug you have to start a remote connection to a debug port.

The default port is 5005, so when using your favorite IDE setup a remote debug connection to localhost at port 5005. 

Create a breakpoint in the processor and wait for the breakpoint to be hit. 

As you can notice, the quarkus hotdeploy comes with a small downside, that is you cannot simply run as debug. You have to first start your quarkus application and then start the debug session. 