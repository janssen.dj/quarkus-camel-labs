package nl.rubix.labs;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.camel.builder.RouteBuilder;

/**
 * A simple {@link RouteBuilder}.
 */
@ApplicationScoped
public class MyFirstRoute extends RouteBuilder {
    
    @Override
    public void configure() throws Exception {
        from("timer:foo")
            .process("process")
            .to("log:example");
    }
}