package nl.rubix.labs;


import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

@Named("process")
@ApplicationScoped
public class MyFirstProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.getIn().setBody("Hello World!");
    }
}
