# lab4
If not working from the labs but your own project first copy the model class (or create it yourself).

Add the following dependencies to your pom.xml;
```xml
    <dependency>
      <groupId>org.apache.camel.quarkus</groupId>
      <artifactId>camel-quarkus-rest</artifactId>
    </dependency>


    <dependency>
      <groupId>org.apache.camel.quarkus</groupId>
      <artifactId>camel-quarkus-servlet</artifactId>
    </dependency>

```

This enables you to add the rest component in your route.
(See https://camel.apache.org/components/latest/rest-component.html for more information)

* First create the restConfiguration (endpoint should include a parameter, hint: beer/{name} )
* Create the rest endpoint 
* Change the processor implementation to return a Beer object
* Use the hot deploy to test

Notice that the return type is of type String and is the object and not the content. We need to marshal the data to json.
* add a marshal to json step
* Don't forget to include the library (https://camel.apache.org/camel-quarkus/latest/reference/extensions/jackson.html)!