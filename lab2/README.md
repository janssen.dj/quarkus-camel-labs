# lab2 project

Notice the empty java directory for it to work we have to add some java code.

* Implement a "RouteBuilder" class (hint: import org.apache.camel.builder.RouteBuilder)
* Add the ApplicationScoped annotation
* Create a simple route with a timer and some log!
* run it with:

```bash
java -jar target/*runner.jar
```