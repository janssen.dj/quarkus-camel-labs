package nl.rubix.labs;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class MyFirstRouteTest {

    @Test
    void runningThisTestInOctoberShouldIssueSecondSemester() {
        given().get("/beer/dirk").then().statusCode(200).body(is("{\"name\":\"dirk\",\"description\":\"delicious beer!\"}"));
    }
}