package nl.rubix.labs;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;

/**
 * A simple {@link RouteBuilder}.
 */
@ApplicationScoped
public class MyFirstRoute extends RouteBuilder {
    
    @Override
    public void configure() throws Exception {
        
        restConfiguration()
            .component("servlet")
            .bindingMode(RestBindingMode.json);


        from("rest:get:beer/{name}")
            .process("process")
            .marshal().json()
            .to("log:example");
    }
}